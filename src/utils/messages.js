/** Mensageria **/
class Messages {
  messages() {
    return {
      _PING: 'API OK =]',
      _API_ERROR: 'Falha ao tentar executar o serviço. Tente novamente mais tarde',
      _API_SUCCESS: 'Sucesso',
      _REQUIRED: 'Campo obrigatório',

      _DB_INSERT: 'Não foi adicionar no momento, tente novamente mais tarde',
      _DB_UPDATE: 'Não foi atualizar no momento, tente novamente mais tarde',

      _NOT_FOUND: '- não localizada(o)',

    };
  }
}

module.exports = new Messages().messages();