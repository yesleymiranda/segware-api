const {_API_ERROR, _API_SUCCESS} = require('../utils/messages');

/** Utilizado para padronizar o retorno da API */
class Response {

  constructor(token_request, execution) {
    this.code = '0000';
    this.message = '';
    this.response = null;
    this.token_request = token_request;
    this.execution = execution;
  }

  setResponse(response) {
    this.response = response;
  }

  setMessage(message) {
    this.message = message;
  }

  setError(err) {
    this.code = err.code || '9999';
    this.message = err.message || _API_ERROR;
    this.response = err.response || {error: err.stack || err};
    return this;
  }

  setSuccess(response) {
    this.code = '0000';
    this.message = _API_SUCCESS;
    this.response = response || {};
    return this;
  }
}

module.exports = Response;
