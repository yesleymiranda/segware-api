const {Model, DataTypes} = require('sequelize');

/** Posts **/
class Posts extends Model {
  static init(sequelize) {
    super.init(
        {
          id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
          description: DataTypes.STRING,
          title: DataTypes.STRING,
          user_name: DataTypes.STRING,
          points: DataTypes.INTEGER,
          created_at: DataTypes.DATE
        },
        {
          modelName: 'posts',
          sequelize,
          freezeTableName: true,
        }
    );
  }
}

module.exports = Posts;
