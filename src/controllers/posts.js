'use strict';

const sequelize = require('../database');

const Logger = new (require('../utils/logger'))(__filename);
const {_DB_INSERT, _NOT_FOUND} = require('../utils/messages');

const Posts = require('../models/Posts');

class PostsController {


  /** Serviço para buscar todas as postagens **/
  async getPosts(req, res) {
    try {

      /** Busca todas postagens **/
      const posts = await Posts.findAll({order:[['created_at','desc']]});

      req.body_response.setSuccess(posts);

      res.status(200).json(req.body_response);

    } catch (e) {
      Logger.error(e.stack || JSON.stringify(e));
      res.status(e.status || 500).json(req.body_response.setError(e));
    }
  }

  /** Serviço para criar nova postagem **/
  async createPost(req, res) {
    try {

      const {titulo, descricao, nome_usuario} = req.body;

      /** Cria nova postagem **/
      const posts = await Posts.create({title: titulo, description: descricao, user_name: nome_usuario});

      req.body_response.setSuccess(posts);

      if (!posts) {
        throw ({status: 400, message: _DB_INSERT});
      }

      res.status(201).json(req.body_response);

    } catch (e) {
      Logger.error(e.stack || JSON.stringify(e));
      res.status(e.status || 500).json(req.body_response.setError(e));
    }
  }

  /** Serviço "upvote" ou "downvote" **/
  async upvotePoint(req, res) {
    try {

      const {postagem_id, acao} = req.body;

      let result;

      /** Atualiza pontuação de acordo com ação **/
      if (acao === 'upvote') {
        result = await Posts.update({points: sequelize.literal(`points + 1`)}, {where: {id: postagem_id}});
      } else {
        result = await Posts.update({points: sequelize.literal(`points - 1`)}, {where: {id: postagem_id}});
      }

      if (!result[0]) {
        throw ({status: 400, message: `Postagem${_NOT_FOUND}`});
      }

      req.body_response.setSuccess({retult: result[0]});

      res.status(200).json(req.body_response);

    } catch (e) {
      Logger.error(e.stack || JSON.stringify(e));
      res.status(e.status || 500).json(req.body_response.setError(e));
    }
  }
}

module.exports = new PostsController();
