const {Sequelize} = require('sequelize');

const env = process.env.NODE_ENV;

const Logger = new (require('../utils/logger'))(__filename);

/** Só para não utilizar o mesmo banco para testes do Jest, o correto seria passar por exemplo via dotenv **/
const path = env === 'test' ? './__tests/database.test.sqlite' : './database.sqlite';

class Database {

  constructor() {
    this.sequelize = new Sequelize({
      dialect: 'sqlite',
      storage: path,
      logging: false,
      operatorsAliases: 0,
      define: {
        timestamps: false,
        underscored: 1,
        underscoredAll: 1,
        defaultScope: {
          attributes: {
            exclude: ['createdAt', 'updatedAt', 'updated_at']
          }
        }
      }
    });
  }

  async testConnection() {
    try {
      await this.sequelize.authenticate().then((res) => Logger.info('Teste conexão com banco SQLite: Sucesso', res));
    } catch (error) {
      Logger.error('Teste conexão com banco SQLite: Falha!', error.stack || JSON.stringify(error));
    }
  }

}

module.exports = new Database();
