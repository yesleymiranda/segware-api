const {validationResult} = require('express-validator');

const {_REQUIRED} = require('../utils/messages');

class Validations {

  /** Retorno 400 - caso algum campo fora das regras **/
  intercept(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      let instructions = [];

      // TODO incluir de acordo a necessidade, para infelizmente o 'express-validator' não existe em PT-Br
      errors.array().forEach((item) => {
        if (item.msg === 'required') {
          instructions.push(`${item.param} - ${_REQUIRED}`);
        } else {
          instructions.push(`${item.param} - ${item.msg}`);
        }
      });

      req.body_response.setMessage(instructions.join(' \\n '));
      return res.status(400).json(req.body_response);
    } else {
      next();
    }
  }


}

module.exports = new Validations();