const {body} = require('express-validator');

class PostsValidations {

  validadeNewPost() {
    return [
      body(['titulo', 'nome_usuario'], 'Tamanho inválido (5 à 100)').isLength({min: 5, max: 100}),
      body(['descricao'], 'Tamanho inválido (10 à 1000)').isLength({min: 10, max: 1000})
    ];
  }

  validadeUpdatePointsPost() {
    return [
      body(['postagem_id', 'acao'], 'required').not().isEmpty(),
      body(['acao'], 'Ação inválida (upvote,downvote)').isIn(['upvote', 'downvote'])
    ];
  }
}

module.exports = new PostsValidations();