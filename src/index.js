'use strict';

const Logger = new (require('./utils/logger'))(__filename);
const Database = require('./database/database');
const App = require('./app');


App.set('port', process.env.PORT || 9052);
App.set('host', process.env.HOST || '0.0.0.0');

Logger.info('Inicializando API server...');

if (process.env.NODE_ENV !== 'test') {
  App.listen(App.get('port'), (error) => {

    if (error) {
      Logger.error(`Falha ao tentar iniciar server`, error.stack || JSON.stringify(error));
    }

    Logger.info(`Sucesso ao iniciar server: [${App.get('host')}:${App.get('port')}]`);

    /** Inica models do sequeliza **/
    require('./database');
  });
}


module.exports = App;