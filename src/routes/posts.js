const router = require('express').Router();

const {validadeNewPost, validadeUpdatePointsPost} = require('../validations/posts');
const {intercept} = require('../validations');

const controller = require('../controllers/posts');

/** Serviço para buscar todas as postagens **/
router.get('/posts', controller.getPosts);

/** Serviço para criar nova postagem **/
router.post('/posts', validadeNewPost(), intercept, controller.createPost);

/** Serviço "upvote" ou "downvote" **/
router.put('/posts/points', validadeUpdatePointsPost(), intercept, controller.upvotePoint);

module.exports = router;