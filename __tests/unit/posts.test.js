const request = require("supertest");
const app = require("../../src");
const Posts = require("../../src/models/Posts");
const clear = require("../clear");

describe("Postagem", () => {

  beforeEach(async () => {
    require('../../src/database');
    await clear();
  });

  it("Post nova postagem", async () => {
    const response = await request(app)
        .post("/api/posts")
        .send({
          titulo: "Minha descrição",
          descricao: "O texto do meu post",
          nome_usuario: "Fernanda"
        });

    expect(response.status).toBe(201);

  });

});