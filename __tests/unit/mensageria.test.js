const messages = require('../../src/utils/messages');

describe("Mensageria", () => {
  Object.keys(messages).forEach((m) => {
    it(`Verifica se todas mensagens retornam string : ${m}`, async () => {
      expect(typeof messages[m]).toBe('string');
    });
  });
});