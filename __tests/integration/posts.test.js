const request = require("supertest");
const app = require("../../src");

describe("Postagem - integração", () => {

  beforeEach(async () => {
    require('../../src/database');
  });


  it("Post uma nova postagem e PUT um 'upvote'", async () => {
    const response = await request(app)
        .post("/api/posts")
        .send({
          titulo: "Minha descrição",
          descricao: "O texto do meu post",
          nome_usuario: "Fernanda"
        });

    const upvote = await request(app)
        .put("/api/posts/points")
        .send({
          postagem_id: response.body.response.id,
          acao: "upvote"
        });

    expect(upvote.status).toBe(200);

  });


});