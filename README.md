
### Para instalar e iniciar a aplicação:

- ##### Deixei rodando em: localhost:9052 (Talvez seja necessario alterar URL no App)
- ##### Instalar pacote:
  - **npm i**
  - ou 
  - **yarn**

- ##### Iniciar o server: 
  - **npm run prod**
  - ou
  - **yarn prod**
  

#### Serviços disponiveis:

- GET http://localhost:9052/api/ping
- GET http://localhost:9052/api/posts - Busca todos os posts
- POST http://localhost:9052/api/posts - Insere um novo post
- PUT http://localhost:9052/api/posts/points - upvote ou downvote um post
